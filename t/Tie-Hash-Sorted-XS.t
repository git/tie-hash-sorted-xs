#!/usr/bin/perl
use strict;
use warnings;

use Test::More tests => 5;
BEGIN { use_ok('Tie::Hash::Sorted::XS') };

tie my %hash, 'Tie::Hash::Sorted::XS';

$hash{Jim} = 5;
$hash{Bob} = 3;
$hash{Anna} = 7;

my $keys = join ' ', keys %hash;

is $keys, 'Anna Bob Jim', 'keys are ordered';
is $hash{Bob}, 3, 'retrieval works';


tie my %refhash, 'Tie::Hash::Sorted::XS', Tree::SizeBalanced::any_int->new(sub { $$a <=> $$b });
$refhash{\5} = 1;
my $three = 3;
$refhash{\$three} = 2;
my $tworef = \2;
$refhash{$tworef} = 3;

my $values = '';
for (keys %refhash) {
	$values .= $refhash{$_} . ' ';
}
chop $values;

my @keys = keys %refhash;
is ref($keys[0]), 'SCALAR', 'non-string keys work';
is $values, '3 2 1', 'values of refhash'
